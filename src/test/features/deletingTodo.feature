@foo
Feature: Todo Application

  Scenario: Deleting a todo
  
    Given the user is on the todo application
    And the user has added several todos
    When the user deletes a todo with the title "Call plumber"    
    Then the todo list should not display "Call plumber"