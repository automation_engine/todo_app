@foo
Feature: Todo Application

  Scenario: Enabling dark mode
  
    Given the user is on the todo application   
    When the user switches to dark mode   
    Then the application interface should change to dark mode