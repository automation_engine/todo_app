@foo
Feature: Todo Application

  Scenario: Enabling light mode
  
    Given the user is on the todo application
    And the todo application is on dark mode   
    When the user switches to light mode   
    Then the application interface should change to light mode