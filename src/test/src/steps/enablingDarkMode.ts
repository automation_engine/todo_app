import { ICustomWorld } from '../support/custom-world';
import { When, Then } from '@cucumber/cucumber';
import { expect } from '@playwright/test';


When('the user switches to dark mode', async function (this: ICustomWorld) {
    await this.page!.locator('div').filter({ hasText: /^My Tasks$/ }).getByRole('button').click();

})

Then('the application interface should change to dark mode', async function (this: ICustomWorld) {
    expect(await this.page!.$eval('body', (body) =>getComputedStyle(body).backgroundColor)).toBe('rgb(26, 27, 30)');

});