import { ICustomWorld } from '../support/custom-world';
import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from '@playwright/test';


Given('the user has added several todos', async function (this: ICustomWorld) {
    
    const page = this.page!
    
    const newTaskButton = page.getByRole('button', { name: 'New Task' });
    const createTaskButton = page.getByText('Create Task')
    const title = page.getByLabel('Title');

    await newTaskButton.click();
    await title.fill('Call plumber');
    await createTaskButton.click();

    await newTaskButton.click();
    await title.fill('Buy groceries');
    await createTaskButton.click();

    await newTaskButton.click();
    await title.fill('Todo 1');
    await createTaskButton.click();

    await newTaskButton.click();
    await title.fill('Todo 2');
    await createTaskButton.click();
    
});

When('the user deletes a todo with the title {string}', async function (this: ICustomWorld, todo) {
    const page = this.page!;
    const deleteButton = page.locator('//*[contains(text(), \"' +todo+ '\")]/.. /button');
    
    await deleteButton.click();
});

Then('the todo list should not display {string}', async function (this: ICustomWorld, todo) {
    const page = this.page!;
    const todos= page.locator('.mantine-Text-root').all;

    await expect(todos).not.toContain(todo);
});