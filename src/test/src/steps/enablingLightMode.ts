import { ICustomWorld } from '../support/custom-world';
import {Given, When, Then } from '@cucumber/cucumber';
import { expect } from '@playwright/test';

Given('the todo application is on dark mode', async function () {
    await this.page!.locator('div').filter({ hasText: /^My Tasks$/ }).getByRole('button').click();
});

When('the user switches to light mode', async function (this: ICustomWorld) {
    await this.page!.locator('div').filter({ hasText: /^My Tasks$/ }).getByRole('button').click();

})

Then('the application interface should change to light mode', async function (this: ICustomWorld) {
    expect(await this.page!.$eval('body', (body) =>getComputedStyle(body).backgroundColor)).toBe('rgb(255, 255, 255)');

});