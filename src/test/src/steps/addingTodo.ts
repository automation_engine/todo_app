import { ICustomWorld } from '../support/custom-world';
import { config } from '../support/config';
import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from '@playwright/test';

Given('the user is on the todo application', async function (this: ICustomWorld) {
    const page = this.page!;
    await page.goto(config.BASE_URL);
})

When('the user adds a new todo with the title {string}', async function (this: ICustomWorld, title) {
    const page = this.page!;
    await page.getByRole('button', { name: 'New Task' }).click();
    await page.getByPlaceholder('Task Title').fill(title);
    await page.getByRole('button', { name: 'Create Task' }).click();
})

Then('the todo list should display {string}', async function (this: ICustomWorld, title) {
    const todoTitle = this.page!.getByText(title).nth(0);
    await expect(todoTitle).toHaveText(title);
});