import { Given, Then, When } from "@cucumber/cucumber"
import { chromium, Page, Browser } from "@playwright/test";

let browser: Browser;
let page: Page;

Given('the user is on the todo application', async function () {
    browser = await chromium.launch({ headless: false });
    page = await browser.newPage();
    await page.goto("http://localhost:3000/todo_react_app")
})

When('the user adds a new todo with the title {string}', async function (string) {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});
Then('the todo list should display {string}', async function (string) {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});