
Feature: Todo Application

  Scenario: Adding a todo
    Given the user is on the todo application
    When the user adds a new todo with the title "Buy groceries"
    Then the todo list should display "Buy groceries"
